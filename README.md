# HUMMR Issue

This is the repository for HUMMR's Issue Tracker.

## ✍ Issues

If you've encountered any issues or have suggestions for improvement while utilizing HUMMR and consulting the [HUMMR User Guide](https://pages.cms.hu-berlin.de/roemeltm/hummr_manual/), you've come to the right place. Your feedback is invaluable in helping us maintain the quality and accuracy of HUMMR. Please take a moment to report any issues you encounter or share your thoughts.

Click here to create a [New Issue](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new). But before you do, we kindly ask that you read through the entire README. This ensures you're familiar with the project's guidelines.

## 💡 Tips for Reporting Issues

When reporting an issue, we kindly ask that you follow the provided issue templates as closely as possible. These templates are designed to guide you in providing the necessary information to help us understand and address the issue effectively.

Please select the appropriate template for the type of issue you're reporting and fill out all the relevant sections. The available template options are:

- [Usage Questions](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new?issuable_template=usage_questions)
- [Installation](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new?issuable_template=installation)
- [Bug Report](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new?issuable_template=bug_report)
- [Suggestion and Enhancements](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new?issuable_template=suggestions_enhancements)
- [Discussion](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new?issuable_template=discussion)
- [User Guide Improvement](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues/new?issuable_template=user_guide_improvement)

> 📝 Note: Providing clear and detailed information in your issue report helps us to quickly identify and resolve the problem.

## ⚠ Before Submitting an Issue

Before submitting a new issue, we kindly ask that you review the [HUMMR User Guide](https://pages.cms.hu-berlin.de/roemeltm/hummr_manual/). The user guide contains valuable information and instructions that may help address your question or concern.

Additionally, please take a moment to check if the issue you're experiencing has already been reported by searching through the existing issues. Click here to see existing [Issues](https://gitlab.com/hummr-dev-team/hummr_issue/-/issues). Duplicate issues can clutter the tracker and delay resolution, so verifying if the issue has already been addressed can help streamline the process.
