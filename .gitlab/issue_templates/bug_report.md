# Bug Report

## Description

[Provide a brief description of the bug you encountered.]

### Steps to Reproduce

1. [First Step]
2. [Second Step]
3. [And so on...]

### Expected Behavior

[Describe what you expected to happen.]

### Actual Behavior

[Describe what actually happened.]

### Additional Information

[Any additional information or context that might be helpful in resolving the issue. Include screenshots or error messages if applicable.]

> Please select appropriate labels in `Labels` option below
>
> Suggested Label: "bug"
