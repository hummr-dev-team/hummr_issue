# Usage Question

## Description

[Please provide a clear and concise description of your question about HUMMR's usage.]

### Code Context

[If applicable, provide relevant snippets of input or describe the context in which you're encountering the issue.]

### Steps Taken

[Describe any steps you've already taken to try to resolve the issue.]

### Expected Behavior

[Explain what you expected to happen when using HUMMR.]

### Actual Behavior

[Describe what actually happened when using HUMMR.]

### Additional Information

[Any additional context or information that might be helpful in understanding and addressing your question. This could include screenshots, error messages, or any other relevant details.]

> Please select appropriate labels in `Labels` option below
>
> Suggested Label: "usage questions"
