# User Guide Improvement

## Description

[Describe the improvements or updates you'd like to see in the HUMMR User Guide.]

### Section

[Specify the section of the User Guide that needs improvements or updates, if applicable.]

### Proposed Change

[Describe the proposed changes or improvements in the User Guide.]

### Additional Information

[Any additional context, details, or screenshots that might be helpful in making improvement in our user guide.]

> Please select appropriate labels in `Labels` option below
>
> Suggested Label: "user guide"
