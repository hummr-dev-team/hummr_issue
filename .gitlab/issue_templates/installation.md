# Installation Issue

## Description

[Please provide a clear and concise description of your installation issue or question.]

### Environment

- **Operating System**: [e.g., Windows, macOS, Linux]

### Steps Taken

[Describe any steps you've already taken to try to install HUMMR. Include any commands executed, error messages received, or troubleshooting steps attempted.]

### Expected Outcome

[Explain what you expected to achieve or happen during the installation process.]

### Current Outcome

[Describe the current behavior or outcome you're experiencing during the installation process. Include any error messages, unexpected behavior, or obstacles you're encountering.]

### Additional Information

[Any additional context, details, or screenshots that might be helpful in understanding and resolving the installation issue.]

> Please select appropriate labels in `Labels` option below
>
> Suggested Label: "installation"
